package br.com.cardevisi.financecalc;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class Calc extends ActionBarActivity implements View.OnClickListener {

    private Button btnCalc;
    private Button btnClean;
    private TextView inputValor;
    private TextView inputJuros;
    private TextView inputMulta;
    private TextView inputDias;
    private TextView fieldResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc);

        btnCalc = (Button) findViewById(R.id.btn_calcular);
        btnCalc.setOnClickListener(this);

        btnClean = (Button) findViewById(R.id.btn_clean);
        btnClean.setOnClickListener(this);

        inputValor = (TextView) findViewById(R.id.input_valor);
        inputJuros = (TextView) findViewById(R.id.input_juros);
        inputMulta = (TextView) findViewById(R.id.input_multa);
        inputDias = (TextView) findViewById(R.id.input_dias);
        fieldResult = (TextView) findViewById(R.id.field_result);
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.btn_calcular){
            double valor =  Double.parseDouble(inputValor.getText().toString());
            double juros =  Double.parseDouble(inputJuros.getText().toString());
            double multa =  Double.parseDouble(inputMulta.getText().toString());
            int dias = Integer.parseInt(inputDias.getText().toString());
            String result = String.valueOf(Calculate.getCalc(valor, juros, multa, dias));
            fieldResult.setText(result);
            Toast.makeText(getBaseContext(), "Click"+result, Toast.LENGTH_SHORT).show();
        } else if(v.getId() == R.id.btn_clean) {
            clearField();
        }
    }

    private void clearField () {
        inputDias.setText(null);
        inputJuros.setText(null);
        inputMulta.setText("");
        inputValor.setText("");
        fieldResult.setText("0,00");
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calc, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
