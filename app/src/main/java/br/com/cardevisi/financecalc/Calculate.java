package br.com.cardevisi.financecalc;

/**
 * Created by Casi on 05/26/2015.
 */
public class Calculate {
    public static double getCalc(double valor, double juros, double multa, int dias) {
       return calculaPorcentagem(valor, multa) + calculaJurosDiario(valor, juros, dias);
    }

    public static double calculaPorcentagem(double valor, double porcentagem) {
        return valor + (valor * porcentagem / 100);
    }

    public static double calculaJurosDiario(double valor, double porcentagem, int dias) {
        return calculaPorcentagem(valor, (dias * porcentagem));
    }
}
